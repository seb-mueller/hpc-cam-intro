# HPC workshop - for University of Cambridge users

This workshop serves an introduction to using the [University of Cambridge HPC](https://www.hpc.cam.ac.uk/).
We also make some notes about the usage of the local HPC at [SLCU](https://www.slcu.cam.ac.uk/), which has a similar setup.
Many of the concepts are general and apply to the usage of any HPC (in particular if they also use the SLURM job scheduler).

The materials should be self-explanatory. If you have questions or suggestions for improvement please [open an issue](https://gitlab.com/slcu/computing/hpc-slcu-workshop/issues).


### Pre-requisites

- An account on the University of Cambridge HPC ([register for an account](https://www.hpc.cam.ac.uk/rcs-application) - please speak with your supervisor beforehand as they will need to authorize your request).
- A computer with all the necessary software installed ([instructions](../setup_instructions.md)).
- Some basic knowledge of the Unix command line is assumed, at least how to navigate through the filesystem (using `cd`). See the [Software Carpentry - Unix Workshop](https://swcarpentry.github.io/shell-novice/) materials for a good introduction.


### Materials

Essential introductory materials:

1. [Getting Started](episodes/00_getting_started.md)
1. [Moving data to and from the HPC](episodes/01_move_data.md)
1. [Submitting your first job](episodes/02_slurm_basics.md)
1. [Managing software](episodes/03_managing_software.md)

Use cases, which reflect typical scenarios for running jobs:

- [Running a single job with multi-threaded software](episodes/04_example1.md)
- Submitting multiple jobs with different inputs/outputs using a pipeline script:
  - [With a `for` loop](episodes/04_example2.md)
  - (recommended) [Using SLURM's job arrays](episodes/04_example3.md)


### (Instructor Notes)

[Curriculum development ideas](curriculum_notes.md)
