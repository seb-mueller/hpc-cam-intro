# SLCU HPC workshop

## goal

Make SLCU users _independent_ at using both the local and UIS HPC.

Side effects:
- people should become competent at using other HPC servers also
- based on these materials, create a quick induction document to new (future) users


## target audience

audience might be mixed:

- mainly biologists, possibly with little programming experience
- more advanced users wanting to make the transition to using the university cluster

Would be good to assess the knowledge and/or interests of people. Could run a survey.
The [carpentries pre-workshop survey](https://www.surveymonkey.com/r/Preview/?sm=V6gQbbOKn3NoPKfYKHjAKu_2BBCdtXXsTS2pf1BIdARccEtJQqlu1KFB2j2TcF0MCn) may be helpful here.

- how often do you use HPC?
- what type of jobs do you usually run? (e.g. bioinformatics pipelines, simulations, ...)
- what is your level of confidence with command line (1-5)
- Are you comfortable using command line tools to copy files between computer and server and back (e.g. `scp` or `rsync`)?
- Do you manage your own software installation or rely on support from HPC maintainers?
  - If yes, what do you use (e.g. `conda`)?
- Are you familiar with the `$PATH` variable and what it refers to?
- Are you familiar with programming concepts such as `for` loops?

## pre-requisites

- Basic knowledge of the Unix command line (assume people have perhaps been using the HPC already)


## practical skills to be taught

- how to setup your environment for working on an HPC:
  - software needed and what its use is: terminal, text editor (optional: sshfs, filezilla)
  - how to login using `ssh`; set password-less login; bonus: set `.ssh/config`
  - how to mount the remote storage on your computer (e.g. for editing files with your local text editor)
  - managing software using conda (just basics)
  - moving files with `rsync` (or `scp`)
- how HPC works:
  - difference between login and compute nodes
  - scheduler - what it is and what it does
  - basics of the filesystem organisation (`/home/hm533`, `/rds/user/hm533`)
  - how to manage input/output from jobs  
- how SLURM works
  - request resources for jobs
  - manage jobs: check queue, cancel jobs
  - getting information about cluster
- use common case studies to illustrate different ways to use SLURM
  - single pipeline, single input (e.g. a bioinformatics pipeline)
  - single pipeline, many inputs (e.g. using a script to launch jobs - automate with "for" loop and `--wrap`)
  - repeat same job many times (e.g. simulations using job arrays)


## challenges

(in order of complexity)

- move data from local to remote machine (and back)
- install `anaconda` (follow online instructions)
- install a bioinformatics software using `conda`
- write a simple script from scratch and submit to SLURM


## dataset

https://hpc-carpentry.github.io/hpc-shell/files/bash-lesson.tar.gz


## resources

- [SLURM cheatsheet](https://slurm.schedmd.com/pdfs/summary.pdf)
- ["HPC Carpentry" curriculum](https://hpc-carpentry.github.io)


## notes

- Advanced topics
  - job dependencies
  - srun
  - GNU parallel
