[^ lesson home](../README.md)  |  [< previous episode](./01_move_data.md)  |  [next episode >](./03_managing_software.md)

----

# Submit a job using SLURM

## Lesson objectives

- Understand the basic concept of how SLURM works
- Submit a simple job
- Understand SLURM environment variables

## Submitting a simple job

To submit a job to SLURM, you need to include your code in a *shell script*.
Options for running the job go at the top of the script using the `#SBATCH` keyword, followed by the SLURM option.
The script is then submitted to SLURM using the `sbatch` program.

Here is an example of a simple job:

```bash
#!/bin/bash

#SBATCH -A <FIXME>
#SBATCH -J simple_job
#SBATCH -D /home/<FIXME>/rds/hpc-work/projects/hpc_workshop
#SBATCH -o scripts/first_job.log
#SBATCH -p skylake         # or skylake-highmem
#SBATCH -c 2               # max 32 CPUs
#SBATCH --mem-per-cpu=1G   # max 6G or 12G for skilake-highmem
#SBATCH -t 00:02:00        # HH:MM:SS

echo "This is output to stdout"

echo "This is re-directed to a file of choice" > first_job_output.txt

# SLURM defines some variables automatically, which can be useful when writing scripts
echo "This is job was assigned $SLURM_CPUS_PER_TASK CPUs."
```

Let's break this down:

1. The first line (`#!/bin/bash`) is called a [_shebang_](https://en.wikipedia.org/wiki/Shebang_(Unix)) and indicates which program should interpret this script. In this case, _bash_ is the interpreter of _shell_ scripts (there's other shell interpreters, but that's beyond what we need to worry about here). Basically just **always have this as the first line of your script**.
2. Lines starting with `#SBATCH` contain options for the _sbatch_ program. These options are:
    - `-A` is the billing account, needed if you're using the Cambridge University HPC. You may have more than one (e.g. if you have different [service levels (SL)](https://docs.hpc.cam.ac.uk/hpc/user-guide/policies.html#service-levels), such as the free "SL3" and paid "SL2"), so pick the right one - check them with `mybalance`.
    - `-J` defines a name for the job.
    - `-D` defines the *working directory* used for this job.
    - `-o` defines the file where the output that would normally be printed on the console is saved in. This is defined in relation to the working directory set above.
    - `-c` is the number of CPUs you want to use for your job.
    - `-t` is the time you need for your job to run. This is not always possible to know in advance, so if you're unsure leave it blank. However, if you know your jobs are short, setting this option will make them start faster.
    - `-p` is the *partition*, needed when using the Cambridge University HPC. There are two of these `skylake` and `skylake-highmem`, which allow a maximum of 6G and 12G or RAM per CPU, respectively
    - `--mem-per-cpu=` defines how much RAM memory you want for your job.
3. The other lines are the commands that we actually want to run.
   - Any output that would have been printed on the console (the first and third `echo` commands) will now be put into the file specified with the `#SBATCH -o` option above.
   - Any output that is redirected to a specific file (the second `echo`) will create that file as normal.
   - There are some shell _environment variables_ which SLURM creates that automatically store values that match the resources we request for our job. In the example above "$SLURM_CPUS_PER_TASK" will contain the value 2 because we requested 2 CPUs (`#SBATCH -c 2`)

**Tip - test your jobs faster:**

`#SBATCH --qos=intr` option can be used when _testing_ scripts. This will allocate a maximum of 1h to your job in the highest priority queue. Only one of these jobs is allowed to run at a time and after the 1h the job will be killed, so it should only be used for _testing_ scripts.


### Challenge

- Create a directory called `scripts` in `~/rds/hpc-work/projects/hpc_workshop`
- Copy the code above and save it in a shell script named `first_job.sh`
  - use the text editor on your machine and save the script using the mounted drive (as covered in the [previous lesson](./01_move_data.md))
  - don't forget to modify the `-A` option with your billing account and adjust the `-o` path with your username
- Submit the script to SLURM using `sbatch`

```bash
# submitting the job
sbatch $HOME/rds/hpc-work/hpc_workshop/scripts/first_job.sh
```

## Checking job status

After submitting a job, you can check its status using:

```bash
# show job status
squeue -u <user>

# show details about the job, such as the working directory and output directories
scontrol show job <jobid>
```

This gives you information about the job's status, `PD` means it's *pending* (waiting in the queue) and `R` means it's *running*.


## Cancel a job

To cancel a job do:

```bash
# replace <JOBID> with the job ID from squeue
scancel <JOBID>
```


## Checking resource usage

To check the statistics about a job:

```bash
sacct -o jobname,account,state,reqmem,maxrss,averss,elapsed -j <JOBID>
```

- `jobname` is the job's name
- `account` is the account used for the job
- `state` gives you the state of the job
- `reqmem` is the memory that you asked for (Mc is MB per core)
- `maxrss` is the maximum memory used during the job *per core*
- `averss` is the average memory used *per core*
- `elapsed` how much time it took to run your job

This can help you determine suitable resources (e.g. RAM, time) next time you run a similar job.

**Note:** this only works on the university HPC, not the SLCU one.

## Check account details

- `mybalance` to check how many hours of usage you have available on your account(s)
- `quota` to check how much disk space you have available in `/home` and `/rds`

**Note:** this only works on the university HPC, not the SLCU one.


### Challenge

- Check how many resources your previous job used.
  - note: if you can't remember the JOBID running `sacct` with no other options will show you which jobs were run by you recently.


## Further resources

- [SLURM cheatsheet](https://slurm.schedmd.com/pdfs/summary.pdf)

----

[^ lesson home](../README.md)  |  [< previous episode](./01_move_data.md)  |  [next episode >](./03_managing_software.md)
