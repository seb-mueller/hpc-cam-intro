[back to lesson home](../README.md)

----

# Use Case: many jobs with arrays

## Lesson objectives

- Understand how SLURM job arrays work
- Use the `$SLURM_ARRAY_TASK_ID` variable together with `srun` to run several jobs in parallel


## Using job arrays

Let's say we wanted to run a stochastic simulation, where we essentially perform the same operation many many times (e.g. 1000 times). Submitting 1000 jobs is a little daunting, even with a *for loop* as [we've done before](04_example2.md). What if we realise there's a mistake in the script? Then we have to cancel all those 1000s of jobs individually!!

Job arrays can be useful for this kind of task.
These can be seen as a collection of jobs that run with identical parameters.

The advantage of using job arrays is that you only submit one job, making it easier to manage.

Job arrays are created with the *SBATCH* option `-a START-FINISH` where *START* and *FINISH* are integers defining the range of array numbers created by SLURM.

SLURM then creates a special variable `$SLURM_ARRAY_TASK_ID` which contains the array's individual job number, which can be useful to use in the script.

Finally, we can run each individual job of the array using the special `srun` program (part of *SLURM*).

Let's see this with an example.

### Running a stochastic simulation

Let's say that we had this *python* script that randomly samples 10 numbers from a normal distribution and calculates its mean. Our intention is to run this script 1000 times to get a sense of the error associated with having a sample size of 10 in some experiment.

We write the script so that we pass a [random seed](https://www.statisticshowto.datasciencecentral.com/random-seed-definition/) to the script from the command line.

Save the following code in a file called `normal_error_sim.py`:

```python
#!/bin/python

import numpy as np
import sys

# set random seed (for reproducibility)
# this is fetched from the command line input
simulation_seed = int(sys.argv[1])
np.random.seed(simulation_seed)

# generate 10 random samples from normal with mean = 0 and sd = 1
sample = np.random.normal(loc = 0, scale = 1, size = 10)

# calculate the mean
sample_mean = np.mean(sample)

# print the result
print(simulation_seed, sample_mean)
```

Then running it as:

```bash
python normal_error_sim.py 133
```

Would result in *133 -0.019442541255346994*.

To run this as a job array, we write a script that uses the `$SLURM_ARRAY_TASK_ID` to define the random seed on our script:

```bash
#!/bin/bash
#SBATCH -A <FIXME>
#SBATCH -D /home/<FIXME>/rds/hpc-work/projects/hpc_workshop
#SBATCH -J simulation
#SBATCH -o scripts/run_simulations.log
#SBATCH -c 1
#SBATCH -t 00:01:00        # HH:MM:SS
#SBATCH -p skylake         # or skylake-highmem
#SBATCH --mem-per-cpu=100M   # max 6G or 12G for skilake-highmem
#SBATCH -a 1-100

# the random seed is defined by the SLURM array number
python scripts/normal_error_sim.py $SLURM_ARRAY_TASK_ID >> normal_sim_output.txt
```

Notes:

- In this case, the output of the simulations would be stored in  `normal_sim_output.txt`.
- Alternatively, you could have defined the output file within the *python* script itself.


### Challenge

The code below modifies our [previous pipeline submission script](04_example2.md) to run as a job array instead of using a *for loop*.

- Study and adjust the code and save it in a new script `$HOME/rds/hpc-work/hpc_workshop/scripts/03_mapping_array.sh`
- Launch the job with `sbatch`

```bash
#!/bin/bash
#SBATCH -A <FIXME>
#SBATCH -D /home/<FIXME>/rds/hpc-work/projects/hpc_workshop
#SBATCH -J mapping_array
#SBATCH -o scripts/03_mapping_array_%a.log
#SBATCH -c 2
#SBATCH -t 00:10:00        # hours:minutes:seconds or days-hours:minutes:seconds
#SBATCH -p skylake         # or skylake-highmem
#SBATCH --mem-per-cpu=1G   # max 6G or 12G for skilake-highmem
#SBATCH -a 1-8

# get the file prefix corresponding to the current array job
# see http://bigdatums.net/2016/02/22/3-ways-to-get-the-nth-line-of-a-file-in-linux/
PREFIX=$(ls data/*_1.fastq | sed 's/_1.fastq//' | head -n $SLURM_ARRAY_TASK_ID | tail -n 1)

# get the sample name (without including the data/ path)
SAMPLE=$(basename $PREFIX)

# define variables that will be input to our pipeline script
READ1="data/${SAMPLE}_1.fastq"
READ2="data/${SAMPLE}_2.fastq"
OUT="alignment_example2/${SAMPLE}.sam"
REF="reference/Drosophila_melanogaster.BDGP6.22.dna.toplevel"

# create output directory
mkdir -p "alignment_example3"

# output some informative messages
echo "The input read files are: $READ1 and $READ2"
echo "The reference genome is: $REF"
echo "Output will go to: $OUT"
echo "Number of CPUs used: $SLURM_CPUS_PER_TASK"

#### Run the commands ####

# Align the reads to the genome
bowtie2 --very-fast -p "$SLURM_CPUS_PER_TASK" -x "$REF" -1 "$READ1" -2 "$READ2" > "$OUT"
```

## Further resources

- [job array documentation](https://slurm.schedmd.com/job_array.html)

----

[back to lesson home](../README.md)
